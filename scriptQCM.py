import csv
import random

def lire_questions(fichier):
    with open(fichier, newline='', encoding='utf-8') as csvfile:
        lecteur = csv.DictReader(csvfile)
        questions = list(lecteur)
    return questions

def afficher_question(question):
    print("Question:", question['Question'])
    reponses = [question['Reponse1'], question['Reponse2'], question['Reponse3']]
    random.shuffle(reponses)

    for index, reponse in enumerate(reponses, start=1):
        print(f"{index}. {reponse}")

    return reponses, question['ReponseCorrecte']

def evaluer_reponse(num_question, reponses, reponse_correcte):
    reponse_correcte = reponse_correcte.strip()  # Supprimer les éventuels espaces

    reponse_utilisateur = input("Votre réponse (entrez le numéro) : ")
    if reponse_utilisateur.isdigit():
        reponse_utilisateur = int(reponse_utilisateur)
        if reponse_utilisateur > 0 and reponse_utilisateur <= len(reponses):
            reponse_utilisateur = reponses[reponse_utilisateur - 1]
        else:
            print("Veuillez entrer un numéro de réponse valide.")
            return evaluer_reponse(num_question, reponses, reponse_correcte)
    else:
        print("Veuillez entrer un numéro valide.")
        return evaluer_reponse(num_question, reponses, reponse_correcte)

    if reponse_utilisateur == reponse_correcte:
        print(f"Question {num_question}: Correct!")
        return 1
    else:
        print(f"Question {num_question}: Incorrect! La bonne réponse était {reponse_correcte}")
        return 0

def passer_qcm(questions):
    score = 0
    for i, question in enumerate(questions, start=1):
        reponses, reponse_correcte = afficher_question(question)
        score += evaluer_reponse(i, reponses, reponse_correcte)
        print()  # Ajouter une ligne vide entre les questions

    print(f"Score final: {score} / {len(questions)}")

if __name__ == "__main__":
    fichier_questions = 'QR.csv'
    questions = lire_questions(fichier_questions)
    passer_qcm(questions)


